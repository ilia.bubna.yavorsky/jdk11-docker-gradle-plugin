**JDK 11 DOCKER GRADLE PLUGIN**

_com.bmuschko.docker-remote-api under the hood_

plugin targets:
1) simplify docker building with jlink and java 11
2) params of plugin useful for building inside organization, without global network connection

WARNING: *.apk installs withot SHA comparing!

Example:
1) Minimum configuration. building context is `/build` dir

`*.jar` - 'fat' jar
```
jdk11DockerConf {
     jdkModulesToInclude = ['java.naming', 'java.xml']
}
 ```
2) Maximum configuration. build context is `/build` dir
```
jdk11DockerConf {
    fromAlpine = "alpine:3.8"
    jdkModulesToInclude = ['java.naming', 'java.xml']
    caCertificates = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/ca-certificates-20171114-r3.apk"
    nghttp2Libs = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/nghttp2-libs-1.32.0-r0.apk"
    libssh2 = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libssh2-1.8.0-r3.apk"
    libcurl = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libcurl-7.61.1-r2.apk"
    curl = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/curl-7.61.1-r1.apk"
    openssl = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/openssl-1.0.2r-r0.apk"
    libssl10 = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libssl1.0-1.0.2r-r0.apk"
    libcrypto10 = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libcrypto1.0-1.0.2r-r0.apk"
    binutils = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/binutils-2.30-r5.apk"
    xz = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/xz-5.2.4-r0.apk"
    xzLibs = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/xz-libs-5.2.4-r0.apk"
    alpineGlibc = "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.29-r0/glibc-2.29-r0.apk"
    gccLibs = "https://archive.archlinux.org/packages/g/gcc-libs/gcc-libs-8.2.1%2B20180831-1-x86_64.pkg.tar.xz"
    zLib = "https://archive.archlinux.org/packages/z/zlib/zlib-1%3A1.2.9-1-x86_64.pkg.tar.xz"
    openJdk = "https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_linux-x64_bin.tar.gz"
    customInstructions = [
            new Dockerfile.AddFileInstruction("distributions/${project.name}-${project.version}.tar", "/app"),
            new Dockerfile.EntryPointInstruction("/app/${project.name}-${project.version}/bin/${project.name}")
    ]
}
```