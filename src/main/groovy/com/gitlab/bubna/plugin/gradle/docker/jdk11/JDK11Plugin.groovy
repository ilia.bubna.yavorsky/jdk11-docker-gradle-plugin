package com.gitlab.bubna.plugin.gradle.docker.jdk11

import com.bmuschko.gradle.docker.tasks.image.Dockerfile
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

class JDK11PluginExtension {
    public String fromAlpine = "alpine:3.8"
    public String caCertificates = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/ca-certificates-20171114-r3.apk"
    public String nghttp2Libs = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/nghttp2-libs-1.32.0-r0.apk"
    public String libssh2 = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libssh2-1.8.0-r3.apk"
    public String libcurl = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libcurl-7.61.1-r2.apk"
    public String curl = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/curl-7.61.1-r2.apk"
    public String openssl = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/openssl-1.0.2r-r0.apk"
    public String libssl10 = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libssl1.0-1.0.2r-r0.apk"
    public String libcrypto10 = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/libcrypto1.0-1.0.2r-r0.apk"
    public String binutils = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/binutils-2.30-r5.apk"
    public String xz = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/xz-5.2.4-r0.apk"
    public String xzLibs = "http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/xz-libs-5.2.4-r0.apk"
    public String alpineGlibc = "https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.29-r0/glibc-2.29-r0.apk"
    public String gccLibs = "https://archive.archlinux.org/packages/g/gcc-libs/gcc-libs-8.2.1%2B20180831-1-x86_64.pkg.tar.xz"
    public String zLib = "https://archive.archlinux.org/packages/z/zlib/zlib-1%3A1.2.9-1-x86_64.pkg.tar.xz"
    public String openJdk = "https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_linux-x64_bin.tar.gz"
    public String[] jdkModulesToInclude

    public List<Dockerfile.Instruction> customInstructions
}

class JDK11Plugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        def confExt = project.extensions.create("jdk11DockerConf", JDK11PluginExtension)

        Task dockerfileTask = project.tasks.find {
            it.class instanceof Dockerfile
        } ?: project.getTasks().create("createDockerfile", Dockerfile.class)
        dockerfileTask.configure {
            doFirst {
                destFile = project.file('build/Dockerfile')
                from confExt.fromAlpine
                addFile(confExt.curl, "/tmp/curl.apk")
                addFile(confExt.caCertificates, "/tmp/caCertificates.apk")
                addFile(confExt.nghttp2Libs, "/tmp/nghttp2Libs.apk")
                addFile(confExt.libssh2, "/tmp/libssh2.apk")
                addFile(confExt.libcurl, "/tmp/libcurl.apk")
                runCommand("""rm -rf /etc/apk/repositories \\
                && apk add --allow-untrusted /tmp/caCertificates.apk \\
                && apk add --allow-untrusted /tmp/nghttp2Libs.apk \\
                && apk add --allow-untrusted /tmp/libssh2.apk \\
                && apk add --allow-untrusted /tmp/libcurl.apk \\ 
                && apk add --allow-untrusted /tmp/curl.apk  \\
                && curl -Ls ${confExt.libssl10} > /tmp/libssl10.apk \\
                && curl -Ls ${confExt.libcrypto10} > /tmp/libcrypto10.apk \\
                && curl -Ls ${confExt.openssl} > /tmp/openssl.apk \\
                && apk add --allow-untrusted /tmp/libssl10.apk /tmp/libcrypto10.apk /tmp/openssl.apk \\
                && curl -Ls ${confExt.binutils} > /tmp/binutils.apk \\
                && apk add --allow-untrusted /tmp/binutils.apk \\
                && curl -Ls ${confExt.xz} > /tmp/xz.apk \\
                && curl -Ls ${confExt.xzLibs} > /tmp/xzLibs.apk \\
                && apk add --allow-untrusted /tmp/xz.apk /tmp/xzLibs.apk \\
                && curl -Ls ${confExt.alpineGlibc} > /tmp/glibc.apk \\
                && apk add --allow-untrusted /tmp/glibc.apk \\
                && curl -Ls ${confExt.gccLibs} -o /tmp/gcc-libs.tar.xz \\
                && mkdir /tmp/gcc \\
                && tar -xf /tmp/gcc-libs.tar.xz -C /tmp/gcc \\
                && mv /tmp/gcc/usr/lib/libgcc* /tmp/gcc/usr/lib/libstdc++* /usr/glibc-compat/lib \\
                && strip /usr/glibc-compat/lib/libgcc_s.so.* /usr/glibc-compat/lib/libstdc++.so* \\
                && curl -Ls ${confExt.zLib} -o /tmp/libz.tar.xz \\
                && mkdir /tmp/libz \\
                && tar -xf /tmp/libz.tar.xz -C /tmp/libz \\
                && mv /tmp/libz/usr/lib/libz.so* /usr/glibc-compat/lib \\
                && apk del binutils \\
                && rm -rf /tmp /var/cache/apk/ \\
                && wget -O openjdk_linux-x64_bin.tar.gz ${confExt.openJdk} || true \\
                && tar -xvzf openjdk_linux-x64_bin.tar.gz \\
                && mv jdk-* jdk \\
                && /jdk/bin/jlink --module-path /jdk/jmods --add-modules ${confExt.jdkModulesToInclude.join(',')} --compress 2 --strip-debug --no-header-files --no-man-pages --output /jlinked \\
                && rm openjdk_linux-x64_bin.tar.gz \\
                && rm -r jdk
                """)
                environmentVariable('JAVA_HOME', '/jlinked')

                confExt.customInstructions.forEach {
                    instruction it.getText()
                }
            }
        }
    }

}
