package com.gitlab.bubna.plugin.gradle.docker.jdk11


import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.*
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

@Ignore
class JDK11Spec extends Specification {
    @Rule
    final TemporaryFolder testProjectDir = new TemporaryFolder()
    def buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
            buildscript {
                repositories { 
                    mavenLocal() 
                    jcenter()
                }
                dependencies {
                    classpath "com.gitlab.bubna:jdk11-docker-gradle-plugin:${System.getenv('PLUGIN_VERSION')}"
                }
            }
            
            apply plugin: 'com.gitlab.bubna.plugin.gradle.docker.jdk11'
            
            jdk11DockerConf {
                jarDist = "libs/*.jar"
                jdkModulesToInclude = ['java.naming', 'java.xml']
            }
        """
    }

    def "result of exec of dockerfile is correct"() {
        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments("cDockerfile", "--debug")
                .withPluginClasspath()
                .withDebug(true)
                .build()
        then:
        true==true
//        result.task(":cDockerfile").outcome == TaskOutcome.UP_TO_DATE
    }
}
